# Create your models here.
from django.db import models
from PIL import Image
from datetime import datetime
from ap_sync_data.models import SyncUpdateModel
from mptt.models import MPTTModel, TreeForeignKey
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from RetailProject import settings


# Create your models here.
class Store(SyncUpdateModel):
    name = models.CharField(max_length=512, null=True, blank=True)
    slug = models.SlugField(max_length=10, unique=True ,db_index=True)
    is_active = models.BooleanField(default=True)
    description =  models.CharField(max_length=512, null=True, blank=True)
    location =  models.CharField(max_length=512, null=True, blank=True)
    created_date   = models.DateTimeField(auto_created=True)
    def thumbnail_directory_path(Client, filename):
        dirname = datetime.now().strftime('%y/%m/%d')
        return 'uploads/order-app/{0}/store/{1}'.format(dirname, filename)

    thumbnail = models.FileField(upload_to=thumbnail_directory_path, verbose_name="Hình ảnh",
                              default='', null=True)
    def __str__(self):
        return f'{self.name}'
    
    def save(self, force_insert=False, force_update=False):
        size = 300, 300
        super(Store, self).save(force_insert, force_update)
        if self.id is not None:
            if self.thumbnail:
                try:
                    image = Image.open(self.thumbnail.name)
                    # image = image.resize((100, 100), Image.ANTIALIAS)
                    image.thumbnail(size, Image.ANTIALIAS)
                    image.save(self.thumbnail.name)
                except IOError:
                    print("Co loi trong qua trinh resize")
class AccessPoint(models.Model):
    UNCONNECTED = 'unconnected'
    UPDATING = 'updating'
    CONNECTED = 'connected'
    DISCONNECTED = 'disconnected'
    UPDATE_FAILED = 'update_failed'
    SYNC_COMPLETED = 'sync_completed'

    STATUS_LABELS = (
        (UNCONNECTED, 'Chưa kết nối'),
        (UPDATING, 'Đang cập nhật'),
        (CONNECTED, 'Đã kết nối'),
        (DISCONNECTED, 'Mất kết nối'),
        (UPDATE_FAILED, 'Cập nhật lỗi'),
        (SYNC_COMPLETED, 'Đồng bộ hóa thành công'),
    )

    STATUS_COLORS = {
        UNCONNECTED: '#795548',
        UPDATING: 'lightblue',
        CONNECTED: 'lightgreen',
        DISCONNECTED: 'orange',
        UPDATE_FAILED: 'red',
        SYNC_COMPLETED: 'green',
    }

    name = models.CharField(max_length=512, null=True, blank=True)
    # store = models.ForeignKey(Store, on_delete=models.CASCADE, null=True, blank=True)
    ip_address = models.CharField(max_length=64, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=32,default=UNCONNECTED, choices=STATUS_LABELS)
    status_description = models.TextField(null=True, blank=True)
    last_online = models.DateTimeField(null=True, blank=True)
    data_version = models.CharField(max_length=64, null=True, blank=True, default=0)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.name} | {self.ip_address}'

    def get_status_color(self):
        return self.STATUS_COLORS[self.status]

class AccessPoint2Store(models.Model):
    access_point = models.ForeignKey(AccessPoint,  on_delete=models.CASCADE, related_name='access_points')
    store = models.ForeignKey(Store, on_delete=models.CASCADE)

class TrackingChangeModel(models.Model):
    def save(self, *args, **kwargs):
        if settings.DJANGO_SERVER_TYPE == 'cloud':
            try: 
                access_2_stores = AccessPoint2Store.objects.filter(store_id=self.store_operate_id)
                for ap2store in access_2_stores:
                    ap = AccessPoint.objects.get(pk=ap2store.access_point.id)
                    ap.data_version = int(ap.data_version) + 1
                    ap.save()
            except:
                pass
        super(TrackingChangeModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True

class MyUserManager(BaseUserManager):
    def create_user(self, username,phone_number=None, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not username:
            raise ValueError("Users must have an username")

        user = self.model(
            username=username,
            phone_number=phone_number
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_staffuser(self, username, password=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            username=username,
        )
        user.is_active=True
        user.is_admin=True
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_superuser(self, username, password=None):
        """
        creates a superuser with email and password
        """
        user = self.create_user(
            username=username,
        )
        user.is_active=True
        user.is_admin=True
        user.set_password(password)
        user.save(using=self._db)
        return user

class CustomUser(AbstractBaseUser,SyncUpdateModel):
    STORE_MANAGER = 'store_manager'
    CUSTOMER = 'customer'
    EMPLOYEE = 'employee'
    
    ROLES_CHOICES = [
        (STORE_MANAGER, 'Quản lý'),
        (CUSTOMER, 'Khách hàng'),
        (EMPLOYEE, 'Nhân viên'),
    ]
    class Meta:
        verbose_name_plural = 'Custom User'

    username = models.CharField(max_length=20, unique=True,null=False, blank=False )
    phone_number = models.CharField(max_length=20, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    role = models.CharField(max_length=20, choices=ROLES_CHOICES, default=CUSTOMER)
    store_operate = models.ForeignKey(Store, on_delete=models.CASCADE, null=True, blank=True)
    device_id = models.CharField(max_length=512, null=True, blank=True)
    objects = MyUserManager()

    USERNAME_FIELD = 'username'
    # REQUIRED_FIELDS = ["username"]

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        # "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        # "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        """Is the user a staff member"""
        return self.is_admin
    
    def save(self, *args, **kwargs):
        # Validate and normalize the phone number before saving
        # parsed_number = phonenumbers.parse(self.phone_number, None)
        # if not phonenumbers.is_valid_number(parsed_number):
        #     raise ValueError("Invalid phone number")
        # self.phone_number = phonenumbers.format_number(
        #     parsed_number, phonenumbers.PhoneNumberFormat.E164
        # )
        super().save(*args, **kwargs)

class ProductCategory(MPTTModel, TrackingChangeModel):
    name = models.CharField(verbose_name='Product category', max_length=150, db_index=True)
    slug = models.SlugField(max_length=150, unique=True ,db_index=True)
    # parent = models.ForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True,on_delete=models.CASCADE)
    store_operate = models.ForeignKey(Store, on_delete=models.CASCADE, null=True, blank=True)

    class MPTTMeta:
        # level_attr = 'mptt_level'
        order_insertion_by = ['parent']

    class Meta:
        unique_together = (('parent', 'slug',))
        verbose_name_plural = 'Product Category'

    def get_slug_list(self):
        try:
            ancestors = self.get_ancestors(include_self=True)
        except:
            ancestors = []
        else:
            ancestors = [i.slug for i in ancestors]
        slugs = []
        for i in range(len(ancestors)):
            slugs.append('/'.join(ancestors[:i + 1]))
        return slugs
    def update(self, other):
        attr_names = ['id', 'name', 'slug', 'parent', 'store_operate']
        for an in attr_names:
            self.__setattr__(an, other.__getattribute__(an))

    def __unicode__(self):
        return '%s' % self.name
    def __str__(self):
        return self.name

class Product(SyncUpdateModel, TrackingChangeModel):
    title = models.CharField(max_length=512, null=False, blank=False)
    store_operate = models.ForeignKey(Store, on_delete=models.CASCADE, null=False, blank=False)
    price = models.FloatField(null=False, blank=False)
    discount = models.FloatField(null=True, blank=True)
    inventory = models.IntegerField(null=True, blank=True, default= 0)
    detail = models.CharField(max_length=1024, null=True, blank=True)
    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE, null=False, blank=False)
    is_active= models.BooleanField(default=True)
    is_show_main_media = models.BooleanField(default=False)
    is_show_list_media = models.BooleanField(default=False)
    def thumbnail_directory_path(Client, filename):
        dirname = datetime.now().strftime('%y/%m/%d')
        return 'uploads/order-app/{0}/product/{1}'.format(dirname, filename)

    thumbnail = models.FileField(upload_to=thumbnail_directory_path, verbose_name="Hình ảnh",
                              default='', null=True)
    def __str__(self):
        return f'{self.title}'
    
    def save(self, force_insert=False, force_update=False):
        size = 300, 300
        super(Product, self).save(force_insert, force_update)
        if self.id is not None:
            # previous = Destination.objects.get(id=self.id)
            # if self.avatar and self.avatar != previous.avatar:
            if self.thumbnail:
                try:
                    image = Image.open(self.thumbnail.name)
                    # image = image.resize((100, 100), Image.ANTIALIAS)
                    image.thumbnail(size, Image.ANTIALIAS)
                    image.save(self.thumbnail.name)
                except IOError:
                    print("Co loi trong qua trinh resize")

class DiscountPackage(SyncUpdateModel, TrackingChangeModel):
    title = models.CharField(max_length=512, null=True, blank=True)
    gift_code = models.CharField(max_length=10, null=True, blank=True, unique=True, default='')
    store_operate = models.ForeignKey(Store, on_delete=models.CASCADE, null=True, blank=True)
    discount = models.IntegerField(null=True, blank=True)
    detail = models.TextField(null=True, blank=True)
    amount = models.IntegerField(null=False, blank=False)
    available = models.IntegerField(null=False, blank=False)
    is_active = models.BooleanField(default=True)
    def thumbnail_directory_path(Client, filename):
        dirname = datetime.now().strftime('%y/%m/%d')
        return 'uploads/order-app/{0}/discount-package/{1}'.format(dirname, filename)

    thumbnail = models.FileField(upload_to=thumbnail_directory_path, verbose_name="Hình ảnh",
                              default='', null=True, blank=True)
    def __str__(self):
        return f'{self.title}'
    
    def save(self, force_insert=False, force_update=False):
        size = 300, 300
        super(DiscountPackage, self).save(force_insert, force_update)
        if self.id is not None:
            # previous = Destination.objects.get(id=self.id)
            # if self.avatar and self.avatar != previous.avatar:
            if self.thumbnail:
                try:
                    image = Image.open(self.thumbnail.name)
                    # image = image.resize((100, 100), Image.ANTIALIAS)
                    image.thumbnail(size, Image.ANTIALIAS)
                    image.save(self.thumbnail.name)
                except IOError:
                    print("Co loi trong qua trinh resize")

class TablesArea(models.Model):
    name = models.CharField(verbose_name='Area name', max_length=150, db_index=True)
    is_available = models.BooleanField(default=True)
    store_operate = models.ForeignKey(Store, on_delete=models.CASCADE, null=True, blank=False)
    def __str__(self):
        return f'{self.name}'   
class Table(SyncUpdateModel,TrackingChangeModel):
    name = models.CharField(verbose_name='Table name', max_length=150, db_index=True)
    number_of_chair = models.IntegerField(null=False, blank=False)
    is_available = models.BooleanField(default=True)
    tables_area = models.ForeignKey(TablesArea, on_delete=models.CASCADE, null=True, blank=True)
    store_operate = models.ForeignKey(Store, on_delete=models.CASCADE, null=True, blank=False)
    def __str__(self):
        return f'{self.name}'
class OrderType(models.Model):
    name = models.CharField(verbose_name='Table name', max_length=150, db_index=True)
    number_of_chair = models.IntegerField(null=False, blank=False)
    is_available = models.BooleanField(default=True)
    def __str__(self):
        return f'{self.name}'

class OrderPlace(models.Model):
    PENDING = 'pending'
    CONFIRMED = 'confirmed'
    SERVING = 'serving'
    FINISHED = 'finished'
    CANCELLED = 'cancelled'
    STATUS_CHOICES = [
        (PENDING, 'Chờ xử lý'),
        (CONFIRMED, 'Đã xác nhận'),
        (SERVING, 'Đang phục vụ'),
        (FINISHED, 'Hoàn tất'),
        (CANCELLED, 'Đã bị hủy'),
    ]
    STATUS_CLASSES = [
        (PENDING, 'bg-danger'),
        (CONFIRMED, 'bg-primary'),
        (SERVING, 'bg-indigo'),
        (FINISHED, 'bg-success'),
        (CANCELLED, 'bg-secondary'),
    ]
    ONSITE= 'onsite'
    DELIVERY= 'delivery'
    ORDER_TYPE_CHOICES= [
        (ONSITE, 'Phục vụ tại chỗ'),
        (DELIVERY, 'Giao hàng'),
    ]
    ONLINE_PAY = 'online_pay'
    SHIP_COD = 'ship_cod'
    ONBOARD = 'onboard'
    PAY_TYPE_CHOICES= [
        ('online_pay', 'Thanh toán online'),
        ('ship_cod', 'Ship COD'),
        ('onboard', 'Thanh toán trực tiếp'),
    ]
    store_operate = models.ForeignKey(Store, on_delete=models.CASCADE, null=True, blank=True)
    customer = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    discount = models.ForeignKey(DiscountPackage, on_delete=models.DO_NOTHING, null=True, blank=True)
    table = models.ForeignKey(Table, on_delete=models.DO_NOTHING, null=True, blank=True, db_constraint=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_paid = models.BooleanField(default=False)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default=PENDING)
    price = models.DecimalField(max_digits=8, decimal_places=2,null=True, blank=True)
    order_type = models.CharField(max_length=20, choices=ORDER_TYPE_CHOICES, default=ONSITE)
    pay_type = models.CharField(max_length=20, choices=PAY_TYPE_CHOICES, default=ONBOARD)
    def __str__(self):
        return f'{self.id}'
    def save(self, *args, **kwargs):
        super(OrderPlace, self).save(*args, **kwargs) 
        return self
    
class OrderPlaceProduct(models.Model):
    order_place = models.ForeignKey(OrderPlace,  on_delete=models.CASCADE, related_name='order_items')
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    amount = models.IntegerField(null=False, blank=False, default=1)
    # price = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    note = models.TextField(null=True, blank=True)

class Notification(models.Model):
    type = models.CharField(verbose_name='Type', max_length=150, blank= False, null = False)
    id_item = models.IntegerField(null=True, blank=True)
    table = models.ForeignKey(Table, on_delete=models.DO_NOTHING, null=True, blank=True, db_constraint=False)
    message = models.CharField(max_length=500, blank= False, null = False)
    is_read = models.BooleanField(blank= False, null = False, default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    store_operate = models.ForeignKey(Store, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f'{self.message}'
    

 