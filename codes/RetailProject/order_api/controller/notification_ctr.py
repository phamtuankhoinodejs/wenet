from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from django.core import serializers

from order_api.models import Notification

def send_to_list_notifications(number_more=0,group_name=''):
    store = group_name.split('_')[1]
    total_noti = (Notification.objects.filter(type = 'order_at_admin',store_operate_id = int(store))).count()
    list_noti = Notification.objects.filter(type = 'order_at_admin',store_operate_id = int(store)).order_by('-id')[:(10+number_more)]
    count_read = (Notification.objects.filter(is_read = False, type = 'order_at_admin',store_operate_id = int(store))).count()
    list_noti_json = serializers.serialize('json', list_noti)
    is_load = bool(total_noti == list_noti.count())
    channel_layer = get_channel_layer()   
    async_to_sync(channel_layer.group_send)(
        group_name,
            {
            'type': 'chat.message',
            'data': {
                'type': 'order_at_admin',
                'count_read': count_read,
                'msg': 'Một đơn hàng vừa được đăng ký',
                'items': list_noti_json,
                'is_init': False,
                'load': is_load
            }
        }
        )    