
import json
from django.http import JsonResponse
from rest_framework import status
import django_filters.rest_framework as filters
from django_filters.rest_framework import DjangoFilterBackend
from django.contrib.auth.decorators import login_required
from rest_framework import viewsets, serializers

from order_api.controller.assistant.authenticated_ast import AllowAnyPutDelete, ManagerOfStorePermission
from order_api.controller.assistant.authenticated_ast import ModelViewSet, AdminRoleFilter, ManagerRoleFilter, CustomerRoleFilter
from order_api.controller.assistant.pagination_ast import CustomPagination
from order_api.models import Product
from order_api.controller.category_ctr import ProductCategorySerializer

class ProductSerializer(serializers.ModelSerializer):
    category = ProductCategorySerializer(many=False)
    class Meta:
        model = Product
        fields = '__all__'

class ProductFilter(filters.FilterSet):
    name = filters.CharFilter(field_name='title', lookup_expr='icontains')
    category = filters.CharFilter(field_name='category__id',lookup_expr='icontains')
    store = filters.CharFilter(field_name='store_operate__id',lookup_expr='exact')
    class Meta:
        model = Product
        fields = ['id']

class ProductViewSet(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = ProductFilter
    pagination_class = CustomPagination

    def get_permissions(self):
        if self.action in ['get','list','retrieve']:
            permission_classes = [AllowAnyPutDelete]
        else:
            permission_classes = [ManagerOfStorePermission]
        return [permission() for permission in permission_classes]

@login_required
def update_inventory(request):
    if request.method == 'POST':
        list_orders =  json.loads(request.POST['list_orders'])
        try:
            for order in list_orders:
                product = Product.objects.get(pk=int(order['id']))
                product.inventory = int(order['inventory']) - int(order['amount'])
                product.save()
            return JsonResponse({'message':'update success'}, status=status.HTTP_200_OK)    
        except:  
            return JsonResponse(status=status.HTTP_404_NOT_FOUND)       
    return JsonResponse(status=status.HTTP_404_NOT_FOUND)