# Generated by Django 4.1.7 on 2023-09-27 07:57

from django.db import migrations, models
import django.db.models.deletion
import order_api.models


class Migration(migrations.Migration):

    dependencies = [
        ('order_api', '0017_orderplace_table'),
    ]

    operations = [
        migrations.AddField(
            model_name='table',
            name='store_operate',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='order_api.store'),
        ),
        migrations.AlterField(
            model_name='discountpackage',
            name='amount',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='discountpackage',
            name='available',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='discountpackage',
            name='thumbnail',
            field=models.FileField(blank=True, default='', null=True, upload_to=order_api.models.DiscountPackage.thumbnail_directory_path, verbose_name='Hình ảnh'),
        ),
    ]
