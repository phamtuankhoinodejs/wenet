# Generated by Django 4.1.7 on 2023-10-02 15:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('order_api', '0018_table_store_operate_alter_discountpackage_amount_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(max_length=150, verbose_name='Type')),
                ('id_item', models.IntegerField(blank=True, null=True)),
                ('message', models.CharField(max_length=500)),
                ('is_read', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('store_operate', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='order_api.store')),
            ],
        ),
    ]
