from django.apps import AppConfig
from django.template.defaulttags import register
from RetailProject import settings
class AdminVoltConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'order_manager'

@register.filter
def get_choices_field_label(choices, key):
    if isinstance(choices, tuple):
        return dict(choices).get(key) or ''
    return choices.get(key) or ''

@register.simple_tag
def get_setting_value(name):
    return  getattr(settings, name, "")