from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordChangeForm, UsernameField, PasswordResetForm, SetPasswordForm
from django.contrib.auth.models import User
from order_api.models import Product, ProductCategory, DiscountPackage, Store, OrderPlace, Table
try:
    from django.utils.translation import ugettext_lazy as _
except ImportError:
    from django.utils.translation import gettext_lazy as _  # Django 4.0.0 and more
from django.forms import ModelForm
from django.db.models import Q


class RegistrationForm(UserCreationForm):
  password1 = forms.CharField(
      label=_("Password"),
      widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}),
  )
  password2 = forms.CharField(
      label=_("Confirm Password"),
      widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Confirm Password'}),
  )

  class Meta:
    model = User
    fields = ('username', 'email', )

    widgets = {
      'username': forms.TextInput(attrs={
          'class': 'form-control',
          'placeholder': 'Username'
      }),
      'email': forms.EmailInput(attrs={
          'class': 'form-control',
          'placeholder': 'example@company.com'
      })
    }

class LoginForm(AuthenticationForm):
  username = UsernameField(label=_("Tài khoản"), widget=forms.TextInput(attrs={"class": "form-control", "placeholder": "Tài khoản"}))
  password = forms.CharField(
      label=_("Mật khẩu"),
      strip=False,
      widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": "Mật khẩu"}),
  )

class UserPasswordResetForm(PasswordResetForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control'
    }))

class UserSetPasswordForm(SetPasswordForm):
    new_password1 = forms.CharField(max_length=50, widget=forms.PasswordInput(attrs={
        'class': 'form-control', 'placeholder': 'New Password'
    }), label="New Password")
    new_password2 = forms.CharField(max_length=50, widget=forms.PasswordInput(attrs={
        'class': 'form-control', 'placeholder': 'Confirm New Password'
    }), label="Confirm New Password")
    

class UserPasswordChangeForm(PasswordChangeForm):
    old_password = forms.CharField(max_length=50, widget=forms.PasswordInput(attrs={
        'class': 'form-control', 'placeholder': 'Old Password'
    }), label='Old Password')
    new_password1 = forms.CharField(max_length=50, widget=forms.PasswordInput(attrs={
        'class': 'form-control', 'placeholder': 'New Password'
    }), label="New Password")
    new_password2 = forms.CharField(max_length=50, widget=forms.PasswordInput(attrs={
        'class': 'form-control', 'placeholder': 'Confirm New Password'
    }), label="Confirm New Password")

class ProductCreateForm(ModelForm):
    def __init__(self, user=None, *args, **kwargs):
        super(ProductCreateForm, self).__init__(*args, **kwargs)
        self.fields['title'].required = False
        self.fields['category'].required = False
        self.fields['price'].required = False
        self.fields['thumbnail'].required = False
        if user :
            self.fields['category'].queryset = ProductCategory.objects.filter(store_operate= user.store_operate, parent__isnull=False)
            self.fields['store_operate'].initial = user.store_operate
    class Meta:
        model = Product
        fields = ('title', 'detail', 'price', 'thumbnail' , 'category', 'store_operate', 'discount', 'is_active',)
        labels = {
            'title': 'Tên sản phẩm',
            'detail': 'Mô tả',
            'category': 'Chủng loại',
            'price': "Giá sản phẩm",
            'is_active': 'Trạng thái',
            'discount': "Giảm giá"
        }
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Tên sản phẩm'
            }),
            'detail': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Mô tả về sản phẩm',
                'rows': '12'
            }),
            'price': forms.NumberInput(attrs={
                'class': 'form-control',
            }),
            'discount': forms.NumberInput(attrs={
                'class': 'form-control',
            }),
            'category': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'Chọn loại sản phẩm',
            }),
            'is_active': forms.CheckboxInput(attrs={
                'class': 'form-check-input',
            }),
            'store_operate': forms.HiddenInput(),
            'thumbnail': forms.FileInput(attrs={
                'class': 'form-control',
                'placeholder': 'Chọn ảnh'
            }),
        }
    def clean(self):
        # data from the form is fetched using super function
        super(ProductCreateForm, self).clean()
         
        title = self.cleaned_data.get('title')
        category = self.cleaned_data.get('category')
        price = self.cleaned_data.get('price')
        thumbnail = self.cleaned_data.get('thumbnail')
        if len(title) < 3:
            self._errors['title'] = self.error_class([
                'Tên sản phẩm không nhỏ hơn 3 kí tự'])
        if len(title) == 0:
            self._errors['title'] = self.error_class([
                'Tên sản phẩm không được để trống'])
        if category is None:
            self._errors['category'] = self.error_class([
                'Loại sản phẩm không được để trống']) 
        if price is None:
            self._errors['price'] = self.error_class([
                'Giá sản phẩm không được để trống']) 
        if thumbnail is None:
            self._errors['thumbnail'] = self.error_class([
                'Chưa chọn ảnh'])     
        # return any errors if found
        return self.cleaned_data    

class OrderCreateForm(ModelForm):
    def __init__(self, user=None, *args, **kwargs):
        super(OrderCreateForm, self).__init__(*args, **kwargs)
        if user :
            self.fields['discount'].queryset = DiscountPackage.objects.filter(store_operate=user.store_operate, is_active=True).filter(~Q(available=0))
            self.fields['store_operate'].initial = user.store_operate
            self.fields['customer'].initial = user
    class Meta:
        model = OrderPlace
        fields = ('customer', 'order_type' , 'pay_type', 'is_paid', 'status', 'store_operate', 'discount')
        labels = {
            'customer': 'Khách hàng',
            'order_type': 'Loại đơn',
            'pay_type': 'Phương thức thanh toán',
            'is_paid': "Đã thanh toán",
            'status': "Trạng thái đơn hàng",
            'discount': "Giảm giá",
        }
        widgets = {
            'status': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'Trạng thái đơn hàng'
            }),
            'discount': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'Chọn mã giảm giá',
                'onchange' : "onDiscountChange()"
            }),
            'order_type': forms.RadioSelect(attrs={
                # 'class': 'd-flex',
                'placeholder': 'Chọn loại đơn hàng'
            }),
            'pay_type': forms.RadioSelect(attrs={
                # 'class': 'form-control',
                'placeholder': 'Chọn phương thức thanh toán'
            }),
            'is_paid': forms.CheckboxInput(attrs={
                'class': 'form-check-input',
            }),
            'store_operate': forms.HiddenInput(),
            'customer': forms.HiddenInput(),
        }

class DiscountPackageCreateForm(ModelForm):
    def __init__(self, user=None, *args, **kwargs):
        super(DiscountPackageCreateForm, self).__init__(*args, **kwargs)
        self.fields['title'].required = False
        self.fields['gift_code'].required = False
        self.fields['thumbnail'].required = False
        self.fields['amount'].required = False
        self.fields['available'].required = False
        if user :
            self.fields['store_operate'].initial = user.store_operate

    class Meta:
        model = DiscountPackage
        fields ='__all__'
        labels = {
            'title': 'Tên gói giảm giá',
            'gift_code': 'Mã giảm giá',
            'detail': 'Chi tiết',
            'discount': "Discount",
            'thumbnail': "Hình ảnh",
            'amount': 'Số lượng',
            'available': 'Còn lại',
            'is_active': 'Is active'
        }
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Tên gói giảm giá'
            }),
            'gift_code': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Mã giảm giá'
            }),
            'detail': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Mô tả',
                'rows': '4'
            }),
            'discount': forms.NumberInput(attrs={
                'class': 'form-control',
            }),
            'amount': forms.NumberInput(attrs={
                'class': 'form-control',
            }),
            'available': forms.NumberInput(attrs={
                'class': 'form-control',
            }),
            'is_active': forms.CheckboxInput(attrs={
                'class': 'form-check-input',
            }),
            'store_operate': forms.HiddenInput(),
            'thumbnail': forms.FileInput(attrs={
                'class': 'form-control',
                'placeholder': 'Chọn ảnh'
            }),
        }
    def clean(self):
        # data from the form is fetched using super function
        super(DiscountPackageCreateForm, self).clean()
         
        title = self.cleaned_data.get('title')
        gift_code = self.cleaned_data.get('gift_code')
        thumbnail = self.cleaned_data.get('thumbnail')
        discount = self.cleaned_data.get('discount')
        amount = self.cleaned_data.get('amount')
        available = self.cleaned_data.get('available')

    
        if title is not None and len(title) < 3:
            self._errors['title'] = self.error_class([
                'Tên nhóm sản phẩm không nhỏ hơn 3 kí tự'])
        if title is None:
            self._errors['title'] = self.error_class([
                'Tên sản phẩm không được để trống'])
            
        if thumbnail is None:
            self._errors['thumbnail'] = self.error_class([
                'Vui lòng chọn ảnh'])

        if gift_code is None:
            self._errors['gift_code'] = self.error_class([
                'Mã giảm giá không được để trống']) 
        if gift_code is not None and len(gift_code) < 2:
            self._errors['gift_code'] = self.error_class([
                'Mã giảm giá không nhỏ hơn 2 kí tự']) 
        if gift_code is not None and len(gift_code) > 12:
            self._errors['gift_code'] = self.error_class([
                'Mã giảm giá không lớn hơn 12 kí tự'])
            
        if discount is None:
            self._errors['discount'] = self.error_class([
                'Giảm giá không được để trống'])    
        if amount is None:
            self._errors['amount'] = self.error_class([
                'Số lượng không được để trống'])  
        if available is None:
            self._errors['available'] = self.error_class([
                'Số lượng còn lại không được để trống'])  
            
        # return any errors if found
        return self.cleaned_data        

class CategoryCreateForm(ModelForm):
    def __init__(self, user=None, required_parent = False, *args, **kwargs):
        super(CategoryCreateForm, self).__init__(*args, **kwargs)
        self.fields['name'].required = False
        self.fields['slug'].required = False
        self.required_parent = required_parent
        if user :
            self.fields['parent'].queryset = ProductCategory.objects.filter(store_operate= user.store_operate, parent__isnull=True)
            self.fields['store_operate'].initial = user.store_operate
    class Meta:
        model = ProductCategory
        fields ='__all__'
        labels = {
            'name': 'Tên nhóm sản phẩm',
            'slug': 'Mã loại sản phẩm',
            'parent': 'Nhóm sản phẩm',
        }
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Tên loại sản phẩm'
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Mã loại sản phẩm'
            }),
             'parent': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'Chọn parent group'
            }),
            'store_operate': forms.HiddenInput(),
        }
    def clean(self):
        # data from the form is fetched using super function
        super(CategoryCreateForm, self).clean()
         
        name = self.cleaned_data.get('name')
        slug = self.cleaned_data.get('slug')
        parent = self.cleaned_data.get('parent')
    
        if len(name) < 3:
            self._errors['name'] = self.error_class([
                'Tên nhóm sản phẩm không nhỏ hơn 3 kí tự'])
        if len(name) == 0:
            self._errors['name'] = self.error_class([
                'Tên sản phẩm không được để trống'])
        if parent is None and self.required_parent == True:
            self._errors['parent'] = self.error_class([
                'Vui lòng chọn nhóm sản phẩm'])
        if len(slug) == 0:
            self._errors['slug'] = self.error_class([
                'Mã sản phẩm không được để trống']) 
         
        # return any errors if found
        return self.cleaned_data       
class StoreCreateForm(ModelForm):
    def __init__(self, user=None, *args, **kwargs):
        super(StoreCreateForm, self).__init__(*args, **kwargs)
        # if user :
        #     self.fields['parent'].queryset = ProductCategory.objects.filter(store_operate= user.store_operate, parent__isnull=True)
        #     self.fields['store_operate'].initial = user.store_operate
    class Meta:
        model = Store
        fields ='__all__'
        labels = {
            'name': 'Tên cửa hàng',
            'created_date': 'Ngày tạo',
            'slug': 'Mã cửa hàng',
            'location': 'Địa chỉ',
            'description': 'Giới thiệu',
            'is_active': 'Trạng thái hoạt động',
            'thumbnail': 'Hình ảnh'
        }
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Tên cửa hàng'
            }),
            'created_date': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Tên cửa hàng',
                'readonly': True
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Mã cửa hàng'
            }),
           'location': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Địa chỉ',
                'rows': '4'
            }),
           'description': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Mô tả',
                'rows': '4'
            }),
            'is_active': forms.CheckboxInput(attrs={
                'class': 'form-check-input',
            }),
            'thumbnail': forms.FileInput(attrs={
                'class': 'form-control',
                'placeholder': 'Hình đại diện'
            }),
        }

class TableCreateForm(ModelForm):
    def __init__(self, user=None, *args, **kwargs):
        super(TableCreateForm, self).__init__(*args, **kwargs)
        self.fields['name'].required = False
        self.fields['number_of_chair'].required = False
        if user :
            self.fields['store_operate'].initial = user.store_operate
    class Meta:
        model = Table
        fields ='__all__'
        labels = {
            'name': 'Tên bàn',
            'number_of_chair': 'Số ghế',
            'is_available': 'Hoạt động',
            'tables_area': 'Khu vực'
        }
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Tên bàn'
            }),
            'number_of_chair': forms.NumberInput(attrs={
                'class': 'form-control',
            }),
            'is_available': forms.CheckboxInput(attrs={
                'class': 'form-check-input',
            }),
            'tables_area': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'Chọn khu vực'
            }),
            'store_operate': forms.HiddenInput(),
        }
    def clean(self):
        super(TableCreateForm, self).clean()
         
        name = self.cleaned_data.get('name')
        tables_area = self.cleaned_data.get('tables_area')
        number_of_chair = self.cleaned_data.get('number_of_chair')

        print('chair', number_of_chair)
    
        
        if len(name) == 0:
            self._errors['name'] = self.error_class([
                'Tên bàn không được để trống'])
        if number_of_chair is None:
            self._errors['number_of_chair'] = self.error_class([
                'Số ghế không được để trống']) 
        if number_of_chair is not None and int(number_of_chair)<=0:
            self._errors['number_of_chair'] = self.error_class([
                'Số ghế không nhỏ hơn 1']) 
        if tables_area is None:
            self._errors['tables_area'] = self.error_class([
                'Khu vực không được để trống'])  
        return self.cleaned_data  