from django.db.models.functions import Cast
from datetime import datetime
from django import urls
from django.shortcuts import render, redirect
import pytz
from order_api.controller.order_place_product_ctr import OrderPlaceProductSerializer
from order_api.controller.store_ctr import StoreSerializer
from order_api.controller.notification_ctr import send_to_list_notifications
from order_manager.forms import RegistrationForm, LoginForm, TableCreateForm, UserPasswordResetForm, UserPasswordChangeForm, UserSetPasswordForm, ProductCreateForm,DiscountPackageCreateForm, CategoryCreateForm, StoreCreateForm, OrderCreateForm
from django.contrib.auth.views import LoginView, PasswordResetView, PasswordChangeView, PasswordResetConfirmView
from django.contrib.auth import logout
from django.core.paginator import Paginator
from django.contrib import messages


from django.contrib.auth.decorators import login_required
from order_api.controller.assistant.decorator import  store_manager_role_required
from order_api.models import Notification, Product, ProductCategory, OrderPlace,  DiscountPackage, Store, OrderPlaceProduct, Table
from django.db.models import Count, Sum, ExpressionWrapper, F
from order_api.controller.order_place_ctr import  OrderPlaceSerializer
from order_api.controller.product_ctr import  ProductSerializer
from django.shortcuts import get_object_or_404
from django.db.models import Q, Value, TextField, FloatField
from django.db.models.functions import Concat
from django.core import serializers
import json

LOGIN_URL="/order-manager/login/"
STATUS_CHOICES = OrderPlace.STATUS_CHOICES
STATUS_CLASSES = OrderPlace.STATUS_CLASSES
ORDER_TYPE_CHOICES = OrderPlace.ORDER_TYPE_CHOICES
PAY_TYPE_CHOICES = OrderPlace.PAY_TYPE_CHOICES

def getItemsWithPagination (request, items):
  LIMIT = 12
  page_number = request.GET.get("page") or 1
  search = request.GET.get("search") or ''
  paginator = Paginator(items, LIMIT)
  page_objects = paginator.get_page(page_number)
  data = {
    'items': enumerate(page_objects),
    'page_objects': page_objects,
    'total': len(items),
    'params': {'page': page_number, 'search':search },
    'from': (int(page_number) - 1) * LIMIT + 1,
    'to': (int(page_number) - 1) * LIMIT + len(page_objects.object_list),
    'total_page': 1 if len(items) <= LIMIT else (len(items) -1)//LIMIT + 1,
    }
  return data

# Dashboard
@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def dashboard(request):
  bestSellerProducts = OrderPlaceProduct.objects.filter(product__store_operate = request.user.store_operate, order_place__is_paid=True).annotate(count=Sum('amount')).annotate(total=ExpressionWrapper(F('count') * F('product__price'), output_field= FloatField() )).order_by('-count')[:5]
  context = {
    'segment': 'dashboard',
    'best_seller': bestSellerProducts,
  }
  return render(request, 'order-manager/pages/dashboard/dashboard.html', context)

def statistic(request):
  context = {
    'segment': 'Thống kê',
  }
  return render(request, 'order-manager/pages/statistic.html', context)

# Pages
@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def customerManager(request):
  context = {
    'segment': 'customers'
  }
  return render(request, 'order-manager/pages/customer/customer-manager.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def categoryManager(request, message=''):
  group  = request.GET.get('group') or ''
  isCheckParent = ProductCategory.objects.filter(parent_id__isnull=True)
  categoriesNoneParent = ProductCategory.objects.filter(store_operate=request.user.store_operate, parent_id__isnull = True).order_by('id')
  categoriesHasParent = ProductCategory.objects.filter(store_operate=request.user.store_operate, parent_id__isnull = False).order_by('id').annotate(number_of_product=Count('product'))
  for ctp in categoriesNoneParent:
    countChildren = 0
    for ctc in categoriesHasParent:
       if ctc.parent_id == ctp.id :
         countChildren = countChildren+1
    ctp.countChildren = countChildren

  if group: 
    categoriesHasParent = categoriesHasParent.filter(parent__id=group)

  context = {
    'segment': 'Quản lý loại sản phẩm','categories_segments' :['Quản lý loại sản phẩm', 'Thêm loại sản phẩm'],
    'parentCategories': ProductCategory.objects.filter(store_operate=request.user.store_operate, parent__isnull = True),
    'data': getItemsWithPagination (request, categoriesNoneParent),
    'dataHasParent': getItemsWithPagination (request, categoriesHasParent),
    'group': group,
    'message': message,
    'parentCount': isCheckParent.count()
  }
  return render(request, 'order-manager/pages/category/category-manager.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def discountPackageManager(request):
  search  = request.GET.get('search') or ''
  packages = DiscountPackage.objects.filter(store_operate=request.user.store_operate).order_by('id')
  if search: 
    packages = packages.filter(title__contains=search)

  context = {
    'segment': 'Gói giảm giá','discount_packages_segments' :['Gói giảm giá', 'Thêm gói giảm giá'],
    'data': getItemsWithPagination (request, packages),
  }
  return render(request, 'order-manager/pages/discount/discount-package-manager.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def productManager(request, message = ''):
  search  = request.GET.get('search') or ''
  category  = request.GET.get('category') or ''
  categories = ProductCategory.objects.filter(store_operate=request.user.store_operate, parent__isnull=False).order_by('id')
  products = Product.objects.filter(store_operate=request.user.store_operate).order_by('id')
  if search:
    products = products.filter(title__contains=search)
  if category:
    products = products.filter(category=category)
  context = {
    'segment': 'Quản lý sản phẩm','product_segments' :['Quản lý sản phẩm', 'Thêm sản phẩm'],
    'data': getItemsWithPagination (request, products),
    'category': category,
    'categories': categories,
    'message': message
  }
  return render(request, 'order-manager/pages/product/product-manager.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def orderManager(request):
  search  = request.GET.get('search') or ''
  status  = request.GET.get('status') or ''
  order_type  = request.GET.get('order_type') or ''
  is_paid = request.GET.get('is_paid') or ''
  orders = OrderPlace.objects.filter(store_operate=request.user.store_operate).annotate(search_order=Concat('store_operate__slug', Value(''), 'id',output_field=TextField())).order_by('-created_at')

  if search:
    orders = orders.filter(Q(customer__username__contains=search) | Q(search_order__contains=search))
  if status:
    orders = orders.filter(status=status)
  if order_type:
    orders = orders.filter(order_type=order_type)
  if is_paid == '1':
    orders = orders.filter(is_paid=True)
  if is_paid == '0':
    orders = orders.filter(is_paid=False)
  serialize = OrderPlaceSerializer(orders, many=True)
  context = {
    'segment': 'Quản lý đơn hàng','orders_segments' :['Quản lý đơn hàng'],
    'data': getItemsWithPagination (request, serialize.data),
    'status': status,
    'order_type': order_type,
    'is_paid': is_paid,
    'STATUS_CHOICES':dict(STATUS_CHOICES),
    'STATUS_CLASSES':dict(STATUS_CLASSES),
    'ORDER_TYPE_CHOICES':dict(ORDER_TYPE_CHOICES),
  }
  return render(request, 'order-manager/pages/order/order-manager.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def orderDetailManager(request):
  orderId  = request.GET.get('id') or ''
  noteId  = request.GET.get('note') or 0
  store = request.user.store_operate.id

  # send notifi  
  if int(noteId) != 0:
    group_name = f"noti_{store}_none"

    res = Notification.objects.filter(id=noteId, is_read=False).update(is_read=True)
    if res == 1 :
      send_to_list_notifications(0, group_name)
    
  order = OrderPlace.objects.get(pk=orderId)
  serializer = OrderPlaceSerializer(order)
  context = {
    'segment': 'Thông tin đơn hàng','orders_segments' :['Quản lý đơn hàng', 'Thông tin đơn hàng'],
    'order_object': order,
    'order': serializer.data,
    'ORDER_TYPE_CHOICES':dict(ORDER_TYPE_CHOICES),
    'PAY_TYPE_CHOICES':dict(PAY_TYPE_CHOICES),
  }
  return render(request, 'order-manager/pages/order/order-detail.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def addProduct(request):
  form = ProductCreateForm(user = request.user)
  if request.method == 'POST':
    form = ProductCreateForm(user = request.user, data = request.POST,files = request.FILES)
    if form.is_valid():
      form.save()
      form = ProductCreateForm(user = request.user, data=None)
      messages.add_message(request, messages.INFO,'Thêm sản phẩm thành công')
      return redirect('products')
    else:
      print('error')
  context = { 'form': form, 'segment': 'Thêm sản phẩm', 'product_segments' :['Quản lý sản phẩm', 'Thêm sản phẩm']   
 }
  return render(request, 'order-manager/pages/product/add-product.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def addOrder(request):
  form = OrderCreateForm(user = request.user)
  store = Store.objects.get(pk=request.user.store_operate.id)
  products = Product.objects.filter(store_operate=request.user.store_operate, is_active=True)
  discounts = DiscountPackage.objects.filter(store_operate=request.user.store_operate, is_active=True).filter(~Q(available=0))
  serialize = serializers.serialize('json', products)
  context = { 
    'form': form, 
    'segment': 'Thêm đơn hàng', 'orders_segments' :['Quản lý đơn hàng', 'Thêm đơn hàng'] ,
    'store': store,
    'discountsSerialize': serializers.serialize('json', discounts),
    'products': products,
    'productsSerialize': serialize
 }
  return render(request, 'order-manager/pages/order/add-order.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def addDiscountPackage(request):
  form = DiscountPackageCreateForm(user = request.user)
  if request.method == 'POST':
    form = DiscountPackageCreateForm(user = request.user, data = request.POST,files = request.FILES)
    if form.is_valid():
      form.save()
      form = DiscountPackageCreateForm(user = request.user, data=None)
      messages.add_message(request, messages.INFO,'Thêm gói giảm giá thành công')
      return redirect('discount_packages')
    else:
      print('error')
  context = { 'form': form, 'segment': 'Thêm gói giảm giá', 'discount_packages_segments' :['Discount packages', 'Thêm gói giảm giá']   
 }
  return render(request, 'order-manager/pages/discount/add-discount-package.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def addCategory(request):
  parent  = request.GET.get('parent') or 0
  form = CategoryCreateForm(user = request.user)
  if request.method == 'POST':
    form = CategoryCreateForm(user = request.user, required_parent=False if int(parent) == 0 else True, data = request.POST,files = request.FILES)
    if form.is_valid():
      form.save()
      form = CategoryCreateForm(user = request.user, data=None)
      messages.add_message(request, messages.INFO,' Cập nhật thông tin thành công')
      return redirect('categories')
    else:
      print('error')
  context = { 'form': form, 
              'segment': 'Thêm nhóm sản phẩm',
              'categories_segments' : ['Quản lý loại sản phẩm','Thêm nhóm sản phẩm'],
              'parent': int(parent)    
            }
  return render(request, 'order-manager/pages/category/add-category.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def editProduct(request):
  id = request.GET.get('id')
  productItem = get_object_or_404(Product, pk=id)
  form = ProductCreateForm(user = request.user, instance= productItem)
  if request.method == 'POST':
    form = ProductCreateForm(user = request.user, data = request.POST,files = request.FILES, instance= productItem)
    if form.is_valid():
      form.save()
      form = ProductCreateForm(user = request.user,  instance= productItem)
      messages.add_message(request, messages.INFO,' Cập nhật thông tin thành công')
      return redirect('products')
    else:
      print('error')
  context = { 'form': form, 'segment': 'Chỉnh sửa sản phẩm', 'product_segments' :['Quản lý sản phẩm', 'Thêm sản phẩm','Chỉnh sửa sản phẩm'] 
 }
  return render(request, 'order-manager/pages/product/edit-product.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def editCategory(request):
  parent  = request.GET.get('parent') or 0
  id = request.GET.get('id')
  categoryItem = get_object_or_404(ProductCategory, pk=id)
  form = CategoryCreateForm(user = request.user, instance= categoryItem)
  if request.method == 'POST':
    form = CategoryCreateForm(user = request.user, required_parent=False if int(parent) == 0 else True, data = request.POST, instance= categoryItem)
    if form.is_valid():
      form.save()
      # form = DiscountPackageCreateForm(user = request.user, data=None)
      messages.add_message(request, messages.INFO,' Cập nhật thông tin thành công')
      return redirect('categories')
    else:
      print('error')
  context = {
     'form': form,'segment': 'Chỉnh sửa loại sản phẩm', 
     'categories_segments' : ['Quản lý loại sản phẩm','Chỉnh sửa loại sản phẩm'],
     'parent': int(parent)
 }
  return render(request, 'order-manager/pages/category/edit-category.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def editDiscountPackage(request):
  id = request.GET.get('id')
  packageItem = get_object_or_404(DiscountPackage, pk=id)
  form = DiscountPackageCreateForm(user = request.user, instance= packageItem)
  if request.method == 'POST':
    form = DiscountPackageCreateForm(user = request.user, data = request.POST,files = request.FILES, instance= packageItem)
    if form.is_valid():
      form.save()
      # form = DiscountPackageCreateForm(user = request.user, data=None)
      messages.add_message(request, messages.INFO,' Cập nhật thông tin thành công')
      return redirect('discount_packages')
    else:
      print('error')
  context = { 'form': form, 'segment': 'Sửa gói giảm giá', 'discount_packages_segments' :['Discount packages', 'Sửa gói giảm giá']   
 }
  return render(request, 'order-manager/pages/discount/edit-discount-package.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def settings(request):
  store = Store.objects.get(pk=request.user.store_operate.id)
  form = StoreCreateForm(user = request.user, instance= store)
  if request.method == 'POST':
    form = StoreCreateForm(user = request.user, data = request.POST,files = request.FILES, instance= store)
    if form.is_valid():
      form.save()
      messages.add_message(request, messages.INFO,' Cập nhật thông tin thành công')
      form = StoreCreateForm(user = request.user, instance= store)
      redirect('/order-manager/settings')
    else:
      print('error')
  context = {
    'segment': 'Thông tin cửa hàng',
    'store': store,
    'form': form,
  }
  return render(request, 'order-manager/pages/settings.html', context)

# Authentication
def register_view(request):
  if request.method == 'POST':
    form = RegistrationForm(request.POST)
    if form.is_valid():
      print("Account created successfully!")
      form.save()
      return redirect('/login/')
    else:
      print("Registration failed!")
  else:
    form = RegistrationForm()
  
  context = { 'form': form }
  return render(request, 'order-manager/accounts/sign-up.html', context)

class UserLoginView(LoginView):
  redirect_field_name="dashboard"
  form_class = LoginForm
  template_name = 'order-manager/accounts/sign-in.html'

class UserPasswordChangeView(PasswordChangeView):
  template_name = 'accounts/password-change.html'
  form_class = UserPasswordChangeForm

class UserPasswordResetView(PasswordResetView):
  template_name = 'accounts/forgot-password.html'
  form_class = UserPasswordResetForm

class UserPasswordResetConfirmView(PasswordResetConfirmView):
  template_name = 'accounts/reset-password.html'
  form_class = UserSetPasswordForm

def logout_view(request):
  logout(request)
  return redirect('/order-manager/login/')

def lock(request):
  return render(request, 'accounts/lock.html')

# Errors
def error_403(request):
  return render(request, 'order-manager/pages/examples/403.html')

def error_404(request, exception=None):
  return render(request, 'order-manager/pages/examples/404.html')

def error_500(request):
  return render(request, 'order-manager/pages/examples/500.html')

# Extra
def upgrade_to_pro(request):
  return render(request, 'order-manager/pages/upgrade-to-pro.html')

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def tableManager(request, message = ''):
  search  = request.GET.get('search') or ''
  tables = Table.objects.filter(store_operate=request.user.store_operate).order_by('id').annotate(count_order = Count('orderplace'))
  host = request.build_absolute_uri('/')
  if search: 
      tables = tables.filter(name__contains=search)
  print('tablelllllllllll', tables)    
  context = {
      'segment': 'Quản lý bàn','table_segments' :['Quản lý bàn', 'Thêm bàn'],
      'store': request.user.store_operate.id,
      'data': getItemsWithPagination(request,tables),
      'host': host
    }
  return render(request, 'order-manager/pages/tables/table-manager.html', context)


@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def addTable(request):
  form = TableCreateForm(user = request.user)
  if request.method == 'POST':
    form = TableCreateForm(user = request.user, data = request.POST)
    if form.is_valid():
      form.save()
      form = TableCreateForm(user = request.user, data=None)
      messages.add_message(request, messages.INFO,'Thêm bàn thành công')
      return redirect('tables')
    else:
      print('error')
  context = { 'form': form,
              'segment': 'Thêm bàn',
              'table_segments' :['Quản lý bàn', 'Thêm bàn']   
            }
  return render(request, 'order-manager/pages/tables/add-table.html', context)

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def editTable(request):
  form = TableCreateForm(user = request.user)
  id = request.GET.get('id')
  tableItem = get_object_or_404(Table, pk=id)
  form = TableCreateForm(user = request.user, instance= tableItem)
  if request.method == 'POST':
    form = TableCreateForm(user = request.user, data = request.POST, instance = tableItem)
    if form.is_valid():
      form.save()
      form = TableCreateForm(user = request.user, data=None)
      messages.add_message(request, messages.INFO,'Cập nhật bàn thành công')
      return redirect('tables')
    else:
      print('error')
  context = { 'form': form,
              'segment': 'Chỉnh sửa bàn',
              'table_segments' :['Quản lý bàn', 'Cập nhật bàn']   
            }
  return render(request, 'order-manager/pages/tables/edit-table.html', context)


@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def warehouseManager(request, message = ''):
  search  = request.GET.get('search') or ''
  during_date_path =  request.GET.get('during_date') or 1
  tz = pytz.timezone('UTC')
  start = datetime.now(tz=tz)
  today = datetime.now(tz=tz)
  if int(during_date_path) == 1 :
    start = today.replace(day=today.day-1,hour=17, minute=0, second=0, microsecond=0)
  elif int(during_date_path) == 2 :
    start = today.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
  elif int(during_date_path) == 3 :
    start = today.replace(day=1, month=1, year=1900 ,hour=0, minute=0, second=0, microsecond=0) 
  else :
    start = today.replace(day=today.day-1,hour=17, minute=0, second=0, microsecond=0)

  orders_today = OrderPlaceProduct.objects.filter(order_place__status = 'finished', order_place__updated_at__gt = start, product__title__contains=search).values(
    'product_id','product__title','product__inventory').annotate(total=Sum('amount')).annotate(
    rate=(Cast(F('total')*100,output_field=FloatField()) / (F('product__inventory') + F('total'))))
  orders_all = OrderPlaceProduct.objects.filter(product__title__contains=search).values(
    'product_id','product__title','product__inventory').annotate(total=Sum(0)).annotate(rate=F('total'))
  orders_no_sale = orders_all
  for product_today in orders_today:
   for product in orders_all:
      if product_today['product_id'] == product['product_id']:
        orders_no_sale = orders_no_sale.exclude(product_id = product_today['product_id'])
  result = orders_today.union(orders_no_sale)

  context = {
      'segment': 'Quản lí kho',
      'table_segments' :['Nhập kho'],
      'store': request.user.store_operate.id,
      'data': result,
      'is_select': during_date_path
    }
  return render(request, 'order-manager/pages/warehouse/warehouse-manager.html', context)
