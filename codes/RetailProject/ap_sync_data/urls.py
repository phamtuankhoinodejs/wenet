from django.urls import path

from .controller import  connect_ctr
from ap_sync_data import views 
urlpatterns = [
    path('connect-server/', views.config_connect),
    path('rq-ap-connect/', connect_ctr.rq_ap_connect),
    path('rq-ap-status/', connect_ctr.rq_ap_status),
]
