class GlobalInfo:
    __instance = None

    @staticmethod
    def get_instance():
        if GlobalInfo.__instance is None:
            GlobalInfo.__instance = GlobalInfo()
        return GlobalInfo.__instance

    def __init__(self):
        self.is_need_reload = False
