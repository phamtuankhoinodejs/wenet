import re, uuid
from uuid import getnode as get_mac

def get_ap_address() :
  # mac = get_mac()
  # print ("The mac address in formatted and less complex way is : ", mac)
  # print ("The MAC address in formatted and less complex way is : ", end="")
  return ':'.join(re.findall('..', '%012x' % uuid.getnode()))