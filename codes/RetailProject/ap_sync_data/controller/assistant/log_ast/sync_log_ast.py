from ap_sync_data.models import SyncLog

def get_client(request):
    if request.user.is_anonymous:
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            client = x_forwarded_for.split(',')[0]
        else:
            client = request.META.get('REMOTE_ADDR')

    else:
        client = request.user.username
    return client


def add_sync_log(action, result):
    sync_log = SyncLog()
    sync_log.action = action
    sync_log.result = result
    sync_log.save()