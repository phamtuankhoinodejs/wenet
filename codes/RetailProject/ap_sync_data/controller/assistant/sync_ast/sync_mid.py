from django.http import Http404
from django.urls import resolve
from ap_sync_data.models import DataDownload


class DataDownloadMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if response.status_code == 404 and 'media/uploads/' in request.get_full_path():
            try:
                resolver_match = resolve(request.path_info)
                if resolver_match.view_name == 'django.views.static.serve':
                    data_download, created = DataDownload.objects.get_or_create(url=request.path_info)
                    if created:
                        data_download.save()
            except Http404:
                pass
        return response
