import urllib.parse


def sync_serial2model(model_objs_serial, target_model):
    for row_serial in model_objs_serial:
        row_serial_obj = row_serial.object
        row_serial_obj.m2m_data = row_serial.m2m_data

        target_obj = target_model()
        target_obj.update(other=row_serial_obj)
        target_obj.save()


def decode_url(url):
    parsed_url = urllib.parse.urlparse(url)

    path = parsed_url.path
    decoded_path = urllib.parse.unquote(path)

    if decoded_path != path:
        decoded_url = urllib.parse.urlunparse(parsed_url._replace(path=decoded_path))
        return decoded_url

    return url
