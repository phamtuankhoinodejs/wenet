import threading
import time

import requests
from django.core import serializers

from ap_sync_data.controller.assistant.sync_ast import sync_update_ast
from ap_sync_data.models import DataDownload
from ap_sync_data.controller.assistant.log_ast import sync_log_ast
from ap_sync_data.models import ConnectConfig, SyncLog

class SyncExecAst:
    __instance = None

    @staticmethod
    def get_instance():
        if SyncExecAst.__instance is None:
            SyncExecAst.__instance = SyncExecAst()
        return SyncExecAst.__instance

    def __init__(self):
        self.current_status = 0
        self.is_running = 0
        self.threader = threading.Thread(target=self.__sync)

        # self.config_obj = ConnectConfig()

    def update_status(self, status):
        self.current_status = status

    def refresh_status(self):
        # update current connect config in AP
        config_obj = ConnectConfig.objects.all()[0]
        is_active, data_version = sync_update_ast.ap_on_cloud_status(config=config_obj)
        config_obj.is_active = is_active
        config_obj.status =  ConnectConfig.CONNECTED if is_active else ConnectConfig.UNCONNECTED
    
        # get new status of AP on cloud
        if is_active and data_version and not data_version == config_obj.data_version :
            # config_obj.data_version = data_version
            self.update_status(status=1)
        # self.config_obj = config_obj
        config_obj.save()

    def __sync(self):
        time.sleep(2)
        print('Start sync ...')
        while self.is_running:
            config_obj = ConnectConfig.objects.all()[0]
            # auto sync
            if self.current_status and config_obj.is_active:
                print('connecting to ... ',self.config_obj.connect_server)
                sync_log_ast.add_sync_log(action=SyncLog.START_UPDATE, result='')
                config_obj.status = ConnectConfig.UPDATING
                data_version, success, msg = sync_update_ast.update_data(config=config_obj)
                if success:
                    print('update done.')
                    sync_log_ast.add_sync_log(action=SyncLog.FINISH_UPDATE, result=SyncLog.SUCCESS)
                    config_obj.status = ConnectConfig.SYNC_COMPLETED
                    config_obj.data_version = data_version
                    self.update_status(0)
                else:
                    print('update fail.')
                    sync_log_ast.add_sync_log(action=SyncLog.FINISH_UPDATE, result=SyncLog.FAILED)
                    config_obj.status = ConnectConfig.UPDATE_FAILED
                    print('message:', msg)
                config_obj.save()
                
            else: 
                print('Nothing to update.')
                self.refresh_status()
            # check if exist file to download
            if DataDownload.objects.count() > 0:
                sync_update_ast.check_download_contents(config=self.config_obj)

            time.sleep(18)

    def __send_logs(self):
        print('start sync logs ...')
        time.sleep(60)

        while True:
            try:
                config_obj = ConnectConfig.objects.all()[0]
                sync_logs = SyncLog.objects.all()
                sync_logs_json = serializers.serialize('json', sync_logs)

                url = config_obj.connect_server + '/rq-ap-logs/'

                data_json = {
                    'account': config_obj.account,
                    'password': config_obj.password,
                    'sync_logs_json': sync_logs_json,
                }

                ret = requests.post(url, json=data_json).json()
                if ret['status']:
                    sync_logs.delete()
                else:
                    print('Logs:', ret['message'])
            except:
                pass
            time.sleep(60)

    def run_sync(self):
        self.is_running = 1
        if not self.threader.is_alive(): 
            self.threader = threading.Thread(target=self.__sync)
            self.threader.start()
        # threading.Thread(target=self.__send_logs).start()

    def stop_sync(self):
        self.is_running = 0
