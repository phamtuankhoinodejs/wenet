import os
import time
# import urllib.request
import datetime
import requests
from django.core import serializers
from django.db import  transaction

from ap_sync_data.controller.assistant.sync_ast import sync_utils
from order_api.models import Product, ProductCategory, CustomUser,Store, DiscountPackage, Table
from ap_sync_data.models import DataDownload
def sync_file(server, url):
    try:
        url = url[1:]
        os.makedirs(os.path.dirname(url), exist_ok=True)
        link = f'{server}{url}'
        # print(datetime.datetime.now(), 'link:', link)

        # urllib.request.urlretrieve(link, url)

        response = requests.get(link)
        if response.status_code == 200:
            with open(url, 'wb') as file:
                file.write(response.content)
                # print(datetime.datetime.now(), 'url:', url)
                return True
    except BaseException as be:
        print('sync file error', be)
    return False


def delete_data():
    Table.objects.all().delete()
    Product.objects.all().delete()
    ProductCategory.objects.all().delete()
    DiscountPackage.objects.all().delete()
    # CustomUser.objects.all().delete()
    # Store.objects.all().delete()

def reset_all():
    delete_data()
    # TemplateDetail.objects.all().delete()

def ap_on_cloud_status(config):
    try:
        url = config.connect_server + 'cloud-manager/ap-status-on-cloud/'
        data = {
            'ip_address': config.ip_address,
        }
        ret = requests.post(url, data=data).json()
        if ret['status']:
            data_version = ret['data']['data_version']
            is_active =  ret['data']['is_active']
            return is_active, data_version
    except:
        pass
    return False, None

@transaction.atomic
def update_data(config):
    target_modes = {
        # 'stores': Store,
        # 'users': CustomUser,
        'discount_packages': DiscountPackage,
        'categories': ProductCategory,
        'products': Product,
        'tables': Table,
    }
    error = ''
    try:
        url = config.connect_server + 'cloud-manager/rq-ap-sync-data/'
        data = {
            'ip_address': config.ip_address
            # 'account': config.account,
            # 'password': config.password,
        }
        response = requests.post(url, data=data, timeout=60).json()
        if response['status']:
            tablesData =  response['data']
            delete_data()
            # time.sleep(10)
            for table in tablesData:
                sync_utils.sync_serial2model(model_objs_serial=serializers.deserialize('json',table[1]), target_model= target_modes[table[0]])
            # time.sleep(2)
            return response['data_version'], True, 'Cập nhật xong'
        return 0, False, response['message']
    
    except BaseException as be:
        error = f'{be}'
        transaction.set_rollback(True)
    return 0 ,False, error

def check_download_contents(config):
    datas_downloads = DataDownload.objects.all()
    for dd in datas_downloads:
        ret = sync_file(server=config.connect_server, url=dd.url)
        if ret:
            dd.delete()
