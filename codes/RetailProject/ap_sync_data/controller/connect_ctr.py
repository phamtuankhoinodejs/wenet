from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
import requests
from datetime import datetime

from ap_sync_data.controller.assistant.sync_ast.sync_exec_ast import SyncExecAst
from ap_sync_data.controller.assistant.log_ast import sync_log_ast
from order_api.controller.assistant.decorator import  store_manager_role_required
from ap_sync_data.models import ConnectConfig, SyncLog
from ap_sync_data.controller.libs.utils import get_ap_address
from ap_sync_data.controller.libs.global_info import GlobalInfo
from ap_sync_data.controller.assistant.sync_ast.sync_exec_ast import SyncExecAst

@login_required
@store_manager_role_required
def rq_ap_connect(request):
    try:
        server = request.POST['server']            
        ip_address = request.POST['ip_address']            
        disconnected = request.POST['disconnected'] == 'true'

        # connect to cloud
        url = server + 'cloud-manager/ap-connect/'
        data = {
            'server': server,
            'ip_address': ip_address,
            'store': request.user.store_operate.id,
            'disconnected': disconnected
        }
        response = requests.post(url, data=data).json()
        try:
            config_obj = ConnectConfig.objects.all()[0]
        except:
            config_obj = ConnectConfig()
            config_obj.ip_address = get_ap_address()

        SyncExecAst.get_instance().config_obj = config_obj
        
        # if has response from request connect
        if response['status']:
            config_obj.connect_server = server
            config_obj.ip_address = ip_address
            config_obj.name = response['data']['name']

            # AP has been disconnect from cloud (is_active false)
            if response['data']['ap_status'] == ConnectConfig.UNCONNECTED:
                config_obj.status = ConnectConfig.UNCONNECTED
                config_obj.save()
                sync_log_ast.add_sync_log(action=SyncLog.CONNECT_TO_SERVER, result=SyncLog.FAILED)
                return JsonResponse({'status': 0, 'message': 'Kết nối thất bại, Access point đã bị tắt ở cloud'})
            else:
                # check if AP disconnected by itself success
                if response['data']['ap_status'] == ConnectConfig.DISCONNECTED :
                    config_obj.status = ConnectConfig.DISCONNECTED
                    config_obj.save()
                    sync_log_ast.add_sync_log(action=SyncLog.DISCONNECT_TO_SERVER, result=SyncLog.SUCCESS)
                    SyncExecAst.get_instance().stop_sync()

                    return JsonResponse({'status': 1, 'message': f'Ngắt kết nối thành công'})
                
                # Ap connect to cloud success
                config_obj.status = ConnectConfig.CONNECTED
                # config_obj.data_version = response['data']['data_version']
                config_obj.save()
 
                SyncExecAst.get_instance().run_sync()
                sync_log_ast.add_sync_log(action=SyncLog.CONNECT_TO_SERVER, result=SyncLog.SUCCESS)

            GlobalInfo.get_instance().is_need_reload = True
            return JsonResponse({'status': 1, 'message': f'Kết nối thành công'})
        else:
            return JsonResponse({'status': 0, 'message': f'{response}'})

    except BaseException as be:
        error = f'({be})'
    return JsonResponse({'status': 0, 'message': f'Kết nối thất bại {error}'})

@login_required
@store_manager_role_required
def rq_ap_status(request):
    try:
        config_obj = ConnectConfig.objects.all()[0]
        sync_logs = SyncLog.objects.all()
        return JsonResponse(
            {
                'status': 1, 
                'data': {
                    'status_color': config_obj.get_status_color(),
                    'status_title': dict(ConnectConfig.STATUS_LABELS).get(config_obj.status),
                    'sync_logs':[
                        {
                            'index': index+1,
                            'updated_date': log.updated_date,
                            'action': dict(SyncLog.ACTION_LABELS).get(log.action),
                            'result': dict(SyncLog.RESULTS_LABELS).get(log.result) if log.result else '',
                            'result_class': 'text-success' if log.result == 'success' else 'text-danger' if log.result == 'failed' else ''
                        } for index, log in enumerate(sync_logs)
                    ]
                    }
            })
    except BaseException as be:
        print(be)
        return JsonResponse({'status': 0})
