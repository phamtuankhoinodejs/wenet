from django.db import models

class ConnectConfig (models.Model):
    UNCONNECTED = 'unconnected'
    UPDATING = 'updating'
    CONNECTED = 'connected'
    DISCONNECTED = 'disconnected'
    UPDATE_FAILED = 'update_failed'
    SYNC_COMPLETED = 'sync_completed'

    STATUS_LABELS = (
        (UNCONNECTED, 'Chưa kết nối'),
        (UPDATING, 'Đang cập nhật'),
        (CONNECTED, 'Đã kết nối'),
        (DISCONNECTED, 'Mất kết nối'),
        (UPDATE_FAILED, 'Cập nhật lỗi'),
        (SYNC_COMPLETED, 'Đồng bộ hóa thành công'),
    )

    STATUS_COLORS = {
        UNCONNECTED: '#795548',
        UPDATING: 'lightblue',
        CONNECTED: 'lightgreen',
        DISCONNECTED: 'orange',
        UPDATE_FAILED: 'red',
        SYNC_COMPLETED: 'green',
    }
    name = models.CharField(max_length=512, null=True, blank=True, default='Chưa đặt tên')
    ip_address = models.CharField(max_length=64, null=True, blank=True)
    connect_server = models.CharField(max_length=64)
    status = models.CharField(max_length=32,default=UNCONNECTED, choices=STATUS_LABELS)
    is_active = models.BooleanField(default=True)
    data_version = models.CharField(max_length=64, null=True, blank=True, default=0)

    def save(self, *args, **kwargs):
        # Override the save method to ensure only one row is created
        self.pk = 1
        super(ConnectConfig, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        # Prevent deletion of the only row
        pass

    def __str__(self):
        return f"Connect Server: {self.connect_server}, Department: {self.department_title}"

    def get_status_color(self):
        return self.STATUS_COLORS[self.status]

class SyncLog(models.Model):
    SUCCESS = 'success'
    FAILED = 'failed'
    CONNECT_TO_SERVER = 'connect_server'
    DISCONNECT_TO_SERVER = 'disconnect_server'
    START_UPDATE = 'start_update'
    FINISH_UPDATE = 'finish_update'
    RESULTS_LABELS = (
        (SUCCESS, 'Thành công'),
        (FAILED, 'Thất bại'),
    )
    ACTION_LABELS = (
        (START_UPDATE, 'Bắt đầu cập nhật'),
        (FINISH_UPDATE, 'Kết thúc cập nhật'),
        (CONNECT_TO_SERVER, 'Kết nối đến Cloud'),
        (DISCONNECT_TO_SERVER, 'Ngắt kết nối'),
    )

    action = models.CharField(max_length=128, null=True, blank=True)
    result = models.CharField(max_length=32,default=SUCCESS, choices=RESULTS_LABELS, null=True, blank=True)
    updated_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-updated_date']

class DataDownload(models.Model):
    url = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.url

class SyncUpdateModel(models.Model):
    def update(self, other):
            attr_names = [f.name for f in self._meta.get_fields() if f.concrete]
            for an in attr_names:
                self.__setattr__(an, other.__getattribute__(an))

    class Meta:
        abstract = True
