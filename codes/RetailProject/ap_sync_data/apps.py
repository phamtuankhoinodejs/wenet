from django.apps import AppConfig


class ApSyncDataConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ap_sync_data'
