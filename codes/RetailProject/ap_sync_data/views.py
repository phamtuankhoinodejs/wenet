from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from order_api.controller.assistant.decorator import  store_manager_role_required
from ap_sync_data.models import ConnectConfig, SyncLog
from ap_sync_data.controller.libs.utils import get_ap_address

@login_required
@store_manager_role_required
def config_connect(request):
    config_db = ConnectConfig.objects.all()
    if len(config_db) > 0:
        config = config_db[0]
    else:
        config = ConnectConfig()
        config.ip_address = get_ap_address()
        config.save()
    sync_logs = SyncLog.objects.all()
    return render(request, 'order-manager/pages/connect-server/connect-server.html', {
        'config': config,
        'sync_logs': sync_logs,
        'segment': 'Cấu hình kết nối',
        'access_points_segments' :['Các điểm truy cập', 'Cấu hình kết nối'],
    })
