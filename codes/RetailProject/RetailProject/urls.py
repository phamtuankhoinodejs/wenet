import os
from django.contrib import admin
from django.urls import path, include, re_path
from django.views.static import serve
from RetailProject import settings
from django.conf.urls.static import static
from order_api.urls import api_router, api_urlpatterns
from django.views.generic import TemplateView
from RetailProject import settings

local_sync_paths = [path('ap-sync/', include('ap_sync_data.urls'))]
cloud_sync_paths = [path('cloud-manager/', include('retail_cloud_manager.urls'))]

urlpatterns = [
    path('', include('order_manager.urls')),
    path('admin/', admin.site.urls),
    path('order-api/', include(api_router.urls )),
    path('order-api/', include(api_urlpatterns)),
    path('order-app/', include('order_app.urls')),
    path('order-manager/', include('order_manager.urls')),
    # path('cloud-manager/', include('retail_cloud_manager.urls')),
    # path('ap-sync/', include('ap_sync_data.urls')),
    path("firebase-messaging-sw.js",
        TemplateView.as_view(
            template_name="firebase-messaging-sw.js",
            content_type="application/javascript",
        ),
        name="firebase-messaging-sw.js"
    ),
    re_path(r'^static/(?P<path>.*)$', serve, {'document_root': os.path.join(settings.BASE_DIR, 'static')}),
    re_path(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
]
if settings.DJANGO_SERVER_TYPE == 'local':
  urlpatterns += local_sync_paths
else:
  urlpatterns += cloud_sync_paths
  
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# handler404 = 'order_manager.views.error_404'
