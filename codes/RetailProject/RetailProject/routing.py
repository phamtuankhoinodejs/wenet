from django.urls import path

from . import consumers

websocket_urlpatterns = [
    path("test-socket/<int:store>/<str:table>/", consumers.NotiConsumer.as_asgi()),
]