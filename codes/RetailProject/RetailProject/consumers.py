import json

from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
from order_api.models import Notification, Store
from django.core import serializers

class NotiConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_store = self.scope["url_route"]["kwargs"]["store"]
        self.room_table = self.scope["url_route"]["kwargs"]["table"]
        self.room_group_name = f"noti_{self.room_store}_{self.room_table}"
        await self.accept()
        await self.channel_layer.group_add(self.room_group_name, self.channel_name)
        res_noti = await self.get_all_noti()
            # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
                 {
                    'type': 'chat.message',
                    'data': res_noti
                }
        )

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(self.room_group_name, self.channel_name)

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        print('send on order status: ',text_data_json)
        # Send message to room group
        if(text_data_json['type'] == 'order_at_app'):
            res_noti = await self.add_noti_update_order(
                text_data_json['order_id'],
                text_data_json['table'],
                text_data_json['status'],
                text_data_json['type'],
                text_data_json['store'],
                )
            await self.channel_layer.group_send(
                self.room_group_name,
                    {
                        'type': 'chat.message',
                        'data': res_noti,
                    }
            )
        if(text_data_json['type'] == 'order_at_admin'): 
               res_noti = await self.get_noti_more(text_data_json['more'], text_data_json['length'])
               await self.channel_layer.group_send(
                self.room_group_name,
                    {
                        'type': 'chat.message',
                        'data': res_noti,
                    }
            ) 

    # Receive message from room group

    async def chat_message(self, event):
        # Send message to WebSocket
        await self.send(text_data=json.dumps({"message": "sent", 'data': event['data']}
        ))

 
    @database_sync_to_async
    def get_all_noti(self):
        if self.room_table == 'none' :
            total_noti = (Notification.objects.filter(type = 'order_at_admin',store_operate_id = int(self.room_store))).count()
            list_noti = Notification.objects.filter(type = 'order_at_admin',store_operate_id = int(self.room_store)).order_by('-id')[:10]
            count_read = (Notification.objects.filter(is_read = False,type = 'order_at_admin',store_operate_id = int(self.room_store))).count()
            list_noti_json = serializers.serialize('json', list_noti)
            is_load = bool(total_noti == list_noti.count())
            print('is loaddddd', is_load)
            return {
                'type':'order_at_admin',
                'count_read': count_read,
                'msg': 'Một đơn hàng vừa được đăng ký',
                'items': list_noti_json,
                'is_init': True,
                'load': is_load
            }
        elif self.room_table == '0' :
            list_noti = Notification.objects.filter(type = 'order_at_app', table_id=None, store_operate_id=int(self.room_store)).order_by('-id')[:10]
            count_read = (Notification.objects.filter(type = 'order_at_app', table_id=None, store_operate_id=int(self.room_store), is_read=False)).count()
            list_noti_json = serializers.serialize('json', list_noti)
            return {
                'type': 'order_at_app',
                'count_read': count_read,
                'msg': 'Một đơn hàng vừa được cập nhật',
                'items': list_noti_json,
                'is_init': True 
            } 
        else :
            list_noti = Notification.objects.filter(type = 'order_at_app', table_id=int(self.room_table), store_operate_id=int(self.room_store)).order_by('-id')[:10]
            count_read = (Notification.objects.filter(type = 'order_at_app', table_id=int(self.room_table), store_operate_id=int(self.room_store), is_read=False)).count()
            list_noti_json = serializers.serialize('json', list_noti)
            return {
                'type': 'order_at_app',
                'count_read': count_read,
                'msg': 'Một đơn hàng vừa được cập nhật',
                'items': list_noti_json,
                'is_init': True
            }   


    @database_sync_to_async
    def add_noti_update_order(self, id_order, table, status, type, store):
        if table == 0 : table = None
        store_noti = Store.objects.get(pk=store)
        msg = ''
        if status == 'confirmed':
            msg = 'Đơn hàng của bạn đã được xác nhận'
        elif status == 'cancelled':
            msg = 'Đơn hàng của bạn đã bị huỷ'
        elif status == 'serving':
            msg = 'Đơn hàng của bạn đã sẵn sàng phục vụ'
        elif status == 'finished':
            msg = 'Đơn hàng của bạn đã hoàn tất'

        noti = Notification.objects.create(
            type=type,
            id_item=id_order,
            table_id=table,
            is_read=False,
            message=msg,
            store_operate=store_noti
        )
        count_read = (Notification.objects.filter(type = 'order_at_app', table_id=table, store_operate_id=store, is_read=False)).count()
        noti_json = serializers.serialize('json', [ noti, ])
        return {
            'type': type,
            'count_read': count_read,
            'msg': 'Một đơn hàng vừa được cập nhật',
            'items': noti_json,
            'is_init': False
        }   
    
    @database_sync_to_async
    def get_noti_more(self, more = 0, length = 0):
        total_noti = (Notification.objects.filter(type = 'order_at_admin',store_operate_id = int(self.room_store))).count()
        list_noti = Notification.objects.filter(type = 'order_at_admin',store_operate_id = int(self.room_store)).order_by('-id')[:(length+more)]
        count_read = (Notification.objects.filter(is_read = False,type = 'order_at_admin',store_operate_id = int(self.room_store))).count()
        list_noti_json = serializers.serialize('json', list_noti)
        is_load = bool(total_noti == list_noti.count())
        return {
            'type':'order_at_admin',
            'count_read': count_read,
            'msg': 'Một đơn hàng vừa được đăng ký',
            'items': list_noti_json,
            'is_init': True,
            'load': is_load
        }
        