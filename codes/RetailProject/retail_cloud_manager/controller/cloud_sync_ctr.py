from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from order_api.controller.assistant.decorator import  store_manager_role_required
from datetime import datetime

from order_api.models import  AccessPoint, AccessPoint2Store
from retail_cloud_manager.models import CloudSyncLog

@login_required
@store_manager_role_required
def rq_aps_status(request):
    try:  
      access_points2store = AccessPoint2Store.objects.filter(store_id=request.user.store_operate_id)
      access_points = [
         {
            'id': item.access_point.id,
            'name': item.access_point.name,
            'status': dict(AccessPoint.STATUS_LABELS).get(item.access_point.status),
            'status_color': item.access_point.get_status_color(),
            'is_active': item.access_point.is_active,
          } for item in access_points2store]
      return JsonResponse({'status': 1, 'data': access_points})
    except BaseException as be:
      return JsonResponse({'status': 0, 'data': []})
       
@login_required
@store_manager_role_required
def rq_ap_status(request):
    id = request.POST['id']
    try:  
      access_point = AccessPoint.objects.get(pk=id)
      sync_logs = CloudSyncLog.objects.filter(access_point_id=id)

      data = {
        'id': access_point.id,
        'name': access_point.name,
        'status': dict(AccessPoint.STATUS_LABELS).get(access_point.status),
        'status_color': access_point.get_status_color(),
        'is_active': access_point.is_active,
        'sync_logs':[
          {
            'index': index+1,
            'updated_date': log.updated_date,
            'action': dict(CloudSyncLog.ACTION_LABELS).get(log.action),
            'result': dict(CloudSyncLog.RESULTS_LABELS).get(log.result),
            'result_class': 'text-success' if log.result == 'success' else 'text-danger' if log.result == 'failed' else ''
          } for index, log in enumerate(sync_logs)
        ]
      }
      return JsonResponse({'status': 1, 'data': data})
    except BaseException as be:
      print(be)
      return JsonResponse({'status': 0, 'data': {}})
       