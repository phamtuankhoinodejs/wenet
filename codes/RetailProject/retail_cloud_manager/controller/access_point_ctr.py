
import django_filters.rest_framework as filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, serializers

from order_api.controller.assistant.authenticated_ast import AllowAnyPutDelete, ModelViewSet, ManagerOfStorePermission
from order_api.controller.assistant.pagination_ast import CustomPagination
from retail_cloud_manager.models import AccessPoint
class AccessPointSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccessPoint
        fields = '__all__'

class AccessPointFilter(filters.FilterSet):
    ip_address = filters.CharFilter(field_name='ip_address', lookup_expr='exact')

    class Meta:
        model = AccessPoint
        fields = ['id']

class AccessPointViewSet(ModelViewSet):
    queryset = AccessPoint.objects.all()
    serializer_class = AccessPointSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = AccessPointFilter
    pagination_class = CustomPagination

    def get_permissions(self):
        if self.action in ['get','list','retrieve']:
            permission_classes = [AllowAnyPutDelete]
        else:
            permission_classes = [ManagerOfStorePermission]
        return [permission() for permission in permission_classes]

    
