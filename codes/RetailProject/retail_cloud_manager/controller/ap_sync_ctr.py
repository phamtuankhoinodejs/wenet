import datetime
import json

from django.core import serializers
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from order_api.controller.assistant.decorator import  store_manager_role_required

from retail_cloud_manager.models import  CloudSyncLog
from order_api.models import Store, DiscountPackage, CustomUser, ProductCategory, Product, Table, AccessPoint, AccessPoint2Store
from retail_cloud_manager.controller.libs.cloud_global_info import CloudGlobalInfo
from retail_cloud_manager.controller.assistant import cloud_sync_log

from django.db.models import Q

@csrf_exempt
# @store_manager_role_required
def ap_connect(request):
    if request.POST['disconnected'] == 'True' :
       return ap_disconnect(request)
    error = ''
    ip_address = request.POST['ip_address']
    id_store =  request.POST['store']
    try:
      store = Store.objects.get(pk=id_store)
    except Store.DoesNotExist as be:
        return JsonResponse({'status': 0, 'message': f'({be})'})
    try:
      access_point = AccessPoint.objects.get(ip_address=ip_address)
    except AccessPoint.DoesNotExist:
        access_point = None
    # check if new AP connect first time
    if access_point is None: 
        access_point = AccessPoint()
        access_point.ip_address = ip_address
        access_point.data_version = 1
        access_point.status = AccessPoint.CONNECTED
        access_point.name = store.name + '_AP_'+ ip_address
        access_point.save()

        access_point_2_store = AccessPoint2Store()
        access_point_2_store.access_point= access_point
        access_point_2_store.store = store
        access_point_2_store.save()

        cloud_sync_log.add_sync_log(action=CloudSyncLog.CONNECT, result=CloudSyncLog.SUCCESS, access_point=access_point)
        CloudGlobalInfo.get_instance().is_need_reload = True
        return JsonResponse({
           'status': 1, 
           'message': 'Kết nối lần đầu thành công',
           'data': {'ap_status': access_point.status, 'data_version': access_point.data_version, 'name': access_point.name}
           })
    else:
        # check if AP has been disconnected by cloud
        if not access_point.is_active:
          cloud_sync_log.add_sync_log(action=CloudSyncLog.CONNECT, result=CloudSyncLog.FAILED, access_point=access_point)
          return JsonResponse({
             'status': 1, 
             'message': 'Access point đã bị tắt', 
             'data': {'ap_status': AccessPoint.UNCONNECTED, 'name': access_point.name}})
        else:
          try:
              access_point.status = AccessPoint.CONNECTED
              access_point.last_online = datetime.datetime.now()
              access_point.save()
              data = {'ap_status': access_point.status, 'data_version': access_point.data_version, 'name': access_point.name}

              cloud_sync_log.add_sync_log(action=CloudSyncLog.CONNECT, result=CloudSyncLog.SUCCESS, access_point=access_point)
              return JsonResponse({'status': 1, 'message': 'Kết nối thành công', 'data': data})
          except BaseException as be:
              error = f'({be})'
    return JsonResponse({'status': 0, 'message': f'Thông tin kết nối không đúng, hãy thử lại. {error}'})

@csrf_exempt
# @store_manager_role_required
def ap_disconnect(request):
    error = ''
    ip_address = request.POST['ip_address']
    id_store =  request.POST['store']
    try:
      store = Store.objects.get(pk=id_store)
    except Store.DoesNotExist as be:
        return JsonResponse({'status': 0, 'message': f'({be})'})
    try:
      access_point = AccessPoint.objects.get(ip_address=ip_address)
    except AccessPoint.DoesNotExist as be:
        return JsonResponse({'status': 0, 'message': f'({be})'})
    try:
        access_point.status = AccessPoint.DISCONNECTED
        access_point.last_online = datetime.datetime.now()
        access_point.save()
        data = {'ap_status': access_point.status, 'data_version': access_point.data_version, 'name': access_point.name}
        cloud_sync_log.add_sync_log(action=CloudSyncLog.DISCONNECT, result=CloudSyncLog.SUCCESS, access_point=access_point)

        return JsonResponse({'status': 1, 'message': 'Ngắt kết nối thành công', 'data': data})
    except BaseException as be:
        error = f'({be})'
    return JsonResponse({'status': 0, 'message': f'Thông tin kết nối không đúng, hãy thử lại. {error}'})

@csrf_exempt
def rq_ap_status(request):
    error = ''
    try:
        ip_address = request.POST['ip_address']
        access_point = AccessPoint.objects.get(ip_address=ip_address)
        return JsonResponse({
           'status': 1, 
           'message': 'request status success', 
           'data': {'data_version': access_point.data_version,
                    'is_active': access_point.is_active}})

    except BaseException as be:
        error = f'({be})'
    return JsonResponse({'status': 0, 'message': f'Thông tin kết nối không đúng, hãy thử lại. {error}'})


@csrf_exempt
def rq_ap_logs(request):
    message = ''
    # try:
    #     rq_data = json.loads(request.body.decode())
    #     account = rq_data['account']
    #     password = rq_data['password']
    #     access_point = AccessPoint.objects.get(account=account, password=password)

    #     signage_logs_json = json.loads(rq_data['signage_logs_json'])
    #     sync_logs_json = json.loads(rq_data['sync_logs_json'])

    #     for signage_log in signage_logs_json:
    #         signage_log_obj = SignageLog()
    #         signage_log_obj.access_point = access_point
    #         signage_log_obj.client = signage_log['fields']['client']
    #         signage_log_obj.log_date = signage_log['fields']['log_date']
    #         signage_log_obj.screen_id = signage_log['fields']['screen']
    #         signage_log_obj.object_name = signage_log['fields']['object_name']
    #         signage_log_obj.action = signage_log['fields']['action']
    #         signage_log_obj.save()

    #     for sync_log in sync_logs_json:
    #         sync_log_obj = SyncLog()
    #         sync_log_obj.access_point = access_point
    #         sync_log_obj.action = sync_log['fields']['action']
    #         sync_log_obj.result = sync_log['fields']['result']
    #         sync_log_obj.updated_date = sync_log['fields']['updated_date']
    #         sync_log_obj.save()

    #     return JsonResponse({'status': 1, 'message': message})

    # except BaseException as be:
    #     message = f'[Error]: {be}'

    return JsonResponse({'status': 0, 'message': message})

@csrf_exempt
def rq_ap_sync_data(request):
    try:
        ip_address = request.POST['ip_address']
        access_point = AccessPoint.objects.get(ip_address=ip_address)
        access_point_2_store = AccessPoint2Store.objects.filter(access_point_id=access_point.id)
        store_ids = [item.store.id for item in access_point_2_store]

        # store_list_obj = Store.objects.filter(pk__in=store_ids)
        # stores_json = serializers.serialize('json', store_list_obj)

        # user_list_obj = CustomUser.objects.filter(Q(store_operate_id__in=store_ids) | Q(store_operate__isnull=True))
        # users_json = serializers.serialize('json', user_list_obj)

        discount_package_list_obj = DiscountPackage.objects.filter(store_operate_id__in=store_ids)
        discount_packages_json = serializers.serialize('json', discount_package_list_obj)

        category_list_obj = ProductCategory.objects.filter(store_operate_id__in=store_ids)
        category_json = serializers.serialize('json', category_list_obj)

        product_list_obj = Product.objects.filter(store_operate_id__in=store_ids)
        products_json = serializers.serialize('json', product_list_obj)

        table_list_obj = Table.objects.filter(store_operate_id__in=store_ids)
        tables_json = serializers.serialize('json', table_list_obj)
        cloud_sync_log.add_sync_log(action=CloudSyncLog.LOAD_SYNC_DATA, result=CloudSyncLog.SUCCESS, access_point=access_point)
        return JsonResponse({
            'status': 1,
            'data_version': access_point.data_version,
            'data':[
                # ('stores', stores_json),
                # ('users', users_json),
                ('discount_packages', discount_packages_json),
                ('categories', category_json),
                ('products', products_json),
                ('tables', tables_json),
                ],
            })
    except BaseException as be:
        cloud_sync_log.add_sync_log(action=CloudSyncLog.LOAD_SYNC_DATA, result=CloudSyncLog.FAILED, access_point=access_point)
        return JsonResponse({'status': 0, 'message': f'[Error]: {be}'})
