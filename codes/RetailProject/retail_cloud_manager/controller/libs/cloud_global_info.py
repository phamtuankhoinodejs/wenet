class CloudGlobalInfo:
    __instance = None

    @staticmethod
    def get_instance():
        if CloudGlobalInfo.__instance is None:
            CloudGlobalInfo.__instance = CloudGlobalInfo()
        return CloudGlobalInfo.__instance

    def __init__(self):
        self.is_need_reload = False
