from django.shortcuts import render, redirect
from django.contrib import messages

from django.contrib.auth.decorators import login_required
from order_api.controller.assistant.decorator import  store_manager_role_required
from retail_cloud_manager.models import  CloudSyncLog
from order_api.models import AccessPoint2Store, AccessPoint
from retail_cloud_manager.form import AccessPointForm
from order_manager.views import error_404
LOGIN_URL="/order-manager/login/"

@login_required(login_url=LOGIN_URL)
@store_manager_role_required
def accessPoints(request):
  access_points = AccessPoint2Store.objects.filter(store_id=request.user.store_operate_id)
  context = {
    'segment': 'Điểm truy cập',
    'access_points_segments' :['Điểm truy cập', 'Cấu hình kết nối'],
    'access_points': access_points,
  }

  return render(request, 'cloud-manager/access-point/access-points.html', context)

@login_required
@store_manager_role_required
def accessPointDetail(request):
    id = request.GET.get('id')
    try:
      access_point_item = AccessPoint.objects.get(pk=id)
    except AccessPoint.DoesNotExist:
        return error_404(request)
    ap_sync_logs = CloudSyncLog.objects.filter(access_point_id=id)
    form = AccessPointForm(user = request.user, instance= access_point_item)
    if request.method == 'POST':
      form = AccessPointForm(user = request.user, data = request.POST, instance= access_point_item)
      if form.is_valid():
        # if form.data['is_active'] :
        #   form.cleaned_data['status'] = AccessPoint.UNCONNECTED
        form.save()
        messages.add_message(request, messages.INFO,' Cập nhật thông tin AP thành công')
        return redirect('access_points')
      else:
          print(form.errors )
    return render(request, 'cloud-manager/access-point/access-point-detail.html', {
      'form': form,
      'access_point_item': access_point_item,
      'ap_sync_logs': ap_sync_logs,
      'segment': 'Thông tin Access point',
      'access_points_segments' :['Các điểm truy cập', 'Thông tin Access point'],
    })
