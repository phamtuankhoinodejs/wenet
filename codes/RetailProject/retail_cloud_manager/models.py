from django.db import models
from order_api.models import AccessPoint

class CloudSyncLog(models.Model):
    CONNECT = 'connect'
    DISCONNECT = 'disconnect'
    LOAD_SYNC_DATA = 'load_sync_data'
    ACTION_LABELS = (
        (CONNECT, 'Kết nối đến cloud'),
        (DISCONNECT, 'Ngắt kết nối'),
        (LOAD_SYNC_DATA, 'Tải dữ liệu mới'),
    )
    SUCCESS = 'success'
    FAILED = 'failed'
    RESULTS_LABELS = (
        (SUCCESS, 'Thành công'),
        (FAILED, 'Thất bại'),
    )
    action = models.CharField(max_length=32,default=SUCCESS, choices=ACTION_LABELS, null=True, blank=True)
    result = models.CharField(max_length=32,default=SUCCESS, choices=RESULTS_LABELS, null=True, blank=True)
    updated_date = models.DateTimeField(auto_now_add=True)
    access_point = models.ForeignKey(AccessPoint,  on_delete=models.CASCADE, null=True, blank=True)
    class Meta:
        ordering = ['-updated_date']