from django.apps import AppConfig


class RetailCloudManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'retail_cloud_manager'
