# Generated by Django 4.1.7 on 2023-10-04 03:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('retail_cloud_manager', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accesspoint',
            name='status',
            field=models.CharField(choices=[('unconnected', 'Chưa kết nối'), ('updating', 'Đang cập nhật'), ('connected', 'Đã kết nối'), ('disconnected', 'Mất kết nối'), ('update_failed', 'Cập nhật lỗi')], default='unconnected', max_length=32),
        ),
    ]
