from django import forms
from django.forms import ModelForm
from retail_cloud_manager.models import AccessPoint
class AccessPointForm(ModelForm):
    def __init__(self, user=None, *args, **kwargs):
        super(AccessPointForm, self).__init__(*args, **kwargs)
    class Meta:
        model = AccessPoint
        fields = ['name','ip_address', 'description', 'last_online', 'is_active' ]
        labels = {
            'name': 'Tên Access point',
            'ip_address': 'Địa chỉ IP',
            'description': 'Mô tả',
            'is_active': 'Trạng thái hoạt động',
        }
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Tên access point'
            }),
            'ip_address': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'IP address',
                'readonly': True
            }),
            'description': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Mô tả'
            }),
            'last_online': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Lần cuối kết nối',
                'readonly': True,
            }),
           'description': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Mô tả',
                'rows': '4'
            }),
            'is_active': forms.CheckboxInput(attrs={
                'class': 'form-check-input',
            }),
            # 'status': forms.HiddenInput(),
        }
