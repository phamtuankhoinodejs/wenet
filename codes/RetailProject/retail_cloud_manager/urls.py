from django.urls import path
from retail_cloud_manager import views
from retail_cloud_manager.controller import ap_sync_ctr, cloud_sync_ctr
from retail_cloud_manager.controller.access_point_ctr import AccessPointViewSet
from rest_framework import routers

cloud_api_router = routers.DefaultRouter()
cloud_api_router.register(r'access_points', AccessPointViewSet)

urlpatterns = [
    # manager on cloud view path
    path('access-points/', views.accessPoints, name="access_points"),
    path('access-point-detail/', views.accessPointDetail, name="access_point_detail"),
    path('rq-aps-status/', cloud_sync_ctr.rq_aps_status, name="access_points_status"),
    path('rq-ap-status/', cloud_sync_ctr.rq_ap_status, name="access_point_detail_status"),
    # from access point request to connect cloud
    path('ap-connect/', ap_sync_ctr.ap_connect),
    path('ap-status-on-cloud/', ap_sync_ctr.rq_ap_status),
    path('rq-ap-sync-data/', ap_sync_ctr.rq_ap_sync_data),
]